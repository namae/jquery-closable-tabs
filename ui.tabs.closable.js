/*!
* hello world
 * Copyright (c) 2010 Andrew Watts
 *
 * Dual licensed under the MIT (MIT_LICENSE.txt)
 * and GPL (GPL_LICENSE.txt) licenses
 *
 * http://github.com/andrewwatts/ui.tabs.closable
 * Modified by David Napierata for use with jQuery 1.9+
 */
(function() {
	var ui_tabs_processTabs = $.ui.tabs.prototype._processTabs;
	$.extend($.ui.tabs.prototype, {
		_processTabs : function() {
			var self = this;
			ui_tabs_processTabs.apply(this, arguments);
			// if closable tabs are enable, add a close button
			if (self.options.closable === true) {
				var closable_lis = this._getList().children('li:visible').filter(function() {
					// return the lis that do not have a close button
					return $('span.ui-icon-circle-close', this).length === 0;
				});
				// append the close button and associated events
				closable_lis.each(function() {
					$(this).append('<a href="#"><span class="ui-icon ui-icon-circle-close"></span></a>').find('a:last > span').hover(function() {
						$(this).css('cursor', 'pointer');
					}, function() {
						$(this).css('cursor', 'default');
					}).on('click', function() {
						var index = self._getList().children('li:visible').index($(this).parent().parent());
						if (index > -1) {
							var eventData = {
								ui : $(self._getList().children('li:visible')[index]).find( "a" )[0],
								panel : self.panels[index]
							};
							// call _trigger to see if remove is allowed
							if (false === self._trigger("closableClick", null, eventData)) {
								return;
							}
							if ((index - 1) >= 0 && $(this).closest('li').attr('aria-selected') == 'true') {
								self._activate(index - 1);
							}
							var panelId = $(this).closest('li').remove().attr("aria-controls");
							$("#" + panelId).remove();
							$(this).closest('div').tabs("refresh");
						}
						// don't follow the link
						return false;
					}).end();

					$(this).find('a:last').click(function() {
						var index = self._getList().children('li:visible').index($(this).parent());
						self._activate(index);
					});
				});
			}
		}
	});

})(jQuery);
